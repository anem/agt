// Nombre de la caché

const cacheName = 'cache-agt-0.0.1';

// Listado de archivos para guardar en caché.

const cacheFiles = [
  './',
  './index.html',
  './scripts/scripts.js',
  './styles/styles.css',
  './images/logo.min.png',
];

// Instalación: crear la caché donde almacenar archivos.

self.addEventListener('install', (evt) => {
  console.log('[Service-Worker] Installing');

  // Esperar a que se abra la caché, entonces guardar todos los archivos.
	evt.waitUntil(
    caches.open(cacheName).then((cache) => {
      console.log('[Service-Worker] Adding', cacheFiles.length, 'files to the cache');
      return cache.addAll(cacheFiles);
    })
	);

  self.skipWaiting();
});

// Cuando llega una solicitud, se pasa a través del service worker, que primero la busca
// en la caché.

self.addEventListener('fetch', event => {
  console.log('[Service-Worker] Fetching', event.request.url);

  // As many [*] characters of any type [.] at the start [^], followed by a group [(  )] of exactly [/api/]
  // with the [\/] escaped, followed by as many [*] characters [.] at the end [$]
  if ( event.request.url.match( '^.*(\/api\/).*$' ) ) {
    console.log('[Service-Worker] Fetching', event.request.url, 'from outside the cache')
    return false;
  }

  event.respondWith(

  	// Buscar el archivo en la caché.
    caches.match(event.request)
    .then(response => {
      if (response) {
        // Si se encuentra (response == True, porque hay un match), devolver el archivo encontrado.
        console.log('[Service-Worker] Found', event.request.url, 'in the cache');
        return response;
      }

      // Si no se ha encontrado, solicitarlo.
      console.log('[Service-Worker] Fetching', event.request.url, 'from outside the cache');
      return fetch(event.request)
      .then(response => {
        if (response.status === 404) {
        	// Si la solicitud da como resultado un error 404, devolver una página específica.
          return caches.match('404.html');
        }

        // Si se recibe de manera correcta, devolverlo tras guardarlo en la caché.
        return caches.open(cacheName)
        .then(cache => {
          console.log('[Service-Worker] Saving resource', event.request.url, 'to the cache');
          cache.put(event.request.url, response.clone());
          return response;
        });
      });
    }).catch(error => {

    	// Si después de todo no funciona, es porque ni está descargado el archivo ni hay conexión.
      console.log('Error,', error);
      return caches.match('pages/offline.html');
    })
  );
});

// Cuando cambia el nombre de caché en uso, eliminar las cachés anteriores.

self.addEventListener('activate', event => {
  const cacheWhitelist = [cacheName];

  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            console.log('[Service-Worker] Deleting obsolete', cacheName)
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

// Escucha para recibir notificaciones push

self.addEventListener('push', function(event) {
  console.log('[Push] Received push notification with content', event.data.text());

  const title = 'Moderación';
  const options = {
    body:   event.data.text(),
    icon:  './images/favicons/icon-144.png',
    badge: './images/favicons/icon-144.png',
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

// Escucha cuando alguien toca la notificación

self.addEventListener('notificationclick', function(event) {
  console.log('[Push] Notification clicked');
  event.notification.close();
});
