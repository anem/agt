
/*
 *
 *   Constant variables
 *
 */

const validAffiliations = ['anem'];
const turnNames = {'i': 'Aclaraciones' , 'ii': 'Alusiones', 'iii': 'Intervenciones', 'iv': 'Comentarios'};
const turnRefreshTime = 10*1000;

/*
 *
 *   Functions to work with cookies
 *
 */

// Save a cookie

function setCookie(name,value,days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; Path=/; SameSite=Strict";
}

// Get the value of a cookie

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

// Delete a cookie (by setting it to expire in the past)

function eraseCookie(name) {   
  document.cookie = name+'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';  
}

// Variable names for the cookies

const cookiePrefix = 'anem_agtmod_';
const cookieLife = 7; // Stay alive for seven days
const cookieUsername = cookiePrefix + 'username';
const cookieUserID = cookiePrefix + 'userID';
const cookieRoomName = cookiePrefix + 'roomName';
const cookiePublicKey = cookiePrefix + 'publicKey';
const cookiePrivateKey = cookiePrefix + 'privateKey';

let userCookies = [cookieUsername, cookieUserID];
let roomCookies = [cookieRoomName, cookiePublicKey, cookiePrivateKey];

// Purge all user cookies

function disconnectUser() {
  userCookies.map(eraseCookie);
  roomCookies.map(eraseCookie);
  location.reload();
};

// Purge all room cookies

function exitRoom() {
  roomCookies.map(eraseCookie);
  location.reload();
};

// Purge all room cookies, and delete the room server-side

function purgeRoom() {
  var roomName = getCookie(cookieRoomName),
      privateKey = getCookie(cookiePrivateKey);

  fetch('/api/delete', fetchOptions('POST', {roomName: roomName, privateKey: privateKey}))
  .then( function(res) {
    switch ( res.status ) {
      case 200:
        exitRoom(); break;

      case 403:
        alert('Wrong room password'); break;
    };
  });
};

/*
 *
 *   SVG icons markup
 *
 */

const iconMicrophone = `<svg class="svg--green" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
    <title>Dar voz a la intervención</title>
    <path fill="currentColor" d="M336 192h-16c-8.84 0-16 7.16-16 16v48c0 74.8-64.49 134.82-140.79 127.38C96.71 376.89 48 317.11 48 250.3V208c0-8.84-7.16-16-16-16H16c-8.84 0-16 7.16-16 16v40.16c0 89.64 63.97 169.55 152 181.69V464H96c-8.84 0-16 7.16-16 16v16c0 8.84 7.16 16 16 16h160c8.84 0 16-7.16 16-16v-16c0-8.84-7.16-16-16-16h-56v-33.77C285.71 418.47 352 344.9 352 256v-48c0-8.84-7.16-16-16-16zM176 352c53.02 0 96-42.98 96-96h-85.33c-5.89 0-10.67-3.58-10.67-8v-16c0-4.42 4.78-8 10.67-8H272v-32h-85.33c-5.89 0-10.67-3.58-10.67-8v-16c0-4.42 4.78-8 10.67-8H272v-32h-85.33c-5.89 0-10.67-3.58-10.67-8v-16c0-4.42 4.78-8 10.67-8H272c0-53.02-42.98-96-96-96S80 42.98 80 96v160c0 53.02 42.98 96 96 96z"></path>
    </svg>`;
const iconTrash = `<svg class="svg--red" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
    <title>Eliminar la intervención de la lista</title> 
    <path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path>
    </svg>`;

/*
 *
 *   Push implementation
 *
 */

const pushButton = document.getElementById('button__subscribe_to_push');
let swRegistration = null;
let pushSubscription = null;

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

function pushAddSubscription() {

  fetch('/api/keys', fetchGetOptions)
  .then(r => r.json())
  .then(function(data) {
    let appPublicKey = data.key;
    let appKey = urlB64ToUint8Array(appPublicKey);

    swRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: appKey
    })
    .then(function(subscription) {
      console.log('[Push] User subscribed:', subscription);
      pushSubscription = subscription;
      updatePushButton();
    })
    .catch(function(err) {
      console.log('[Push] Failed to subscribe the user:', err);
      updatePushButton();
    });

  });
};

function pushRemoveSubscription() {

  if ( pushSubscription === null ) {
    console.log('[Push] User is not subscribed');
  } else {
    pushSubscription.unsubscribe()
    .then(() => {
      pushSubscription = null;
      console.log('[Push] User unsubscribed');
      updatePushButton();
    }).catch(e => {
      console.log(e);
      updatePushButton();
    });
  };
};

function updatePushButton() {

  if ( Notification.permission === 'denied' ) {
    pushButton.textContent = 'Notificaciones bloqueadas';
    pushButton.disabled = true;
    return;
  };

  if ( pushSubscription === null ) {
    pushButton.textContent = 'Activar notificaciones';
  } else {
    pushButton.textContent = 'Desactivar notificaciones';
  };

  pushButton.disabled = false;
};

function analyzePush() {

  console.log('[Push] Analyzing push registration');
  pushButton.addEventListener('click', function() {
    pushButton.disabled = true;

    if ( pushSubscription === null ) {
      pushAddSubscription();
    } else {
      pushRemoveSubscription();
    };
  });
  swRegistration.pushManager.getSubscription().then(function(subscription) {
    pushSubscription = subscription;

    if ( pushSubscription === null ) {
      console.log('[Push] User is not subscribed');
    } else {
      console.log('[Push] User is subscribed');
    }

    updatePushButton();
  });
};

if ('serviceWorker' in navigator && 'PushManager' in window) {
  navigator.serviceWorker.register('../service-worker.js')
  .then(function(swReg) {
    console.log('[Service-Worker] Service Worker is registered:', swReg);

    swRegistration = swReg;
    analyzePush();
  })
  .catch(function(error) {
    console.error('[Service-Worker] Service Worker registration error:', error);
  });
} else {
  console.warn('[Push] Not supported');
  pushButton.display = 'none';
};

/*
 *
 *   Layout update
 *
 */

function setUserProfile(username, userID) {
  if ( username.includes('_') ) {
    var usernameAffiliation = username.split('_')[0].toLowerCase();
    var usernameProperName = username.split('_')[1];
    document.getElementById('text__username').innerHTML = usernameProperName;

    if ( validAffiliations.includes(usernameAffiliation) ) {
      document.getElementById('image__username_affiliation').src = './images/logos/logo.' + usernameAffiliation + '.png';
    };
  } else {
    document.getElementById('text__username').innerHTML = username;
  };
  document.getElementById('profile__container').style.display = 'flex';
  document.getElementById('text__user_id').innerHTML = getCookie(cookieUserID);
  document.getElementById('button__disconnect').addEventListener('click', disconnectUser);
};

function printUserProfile(username) {
  var brokenUsername = username.split('_');
  if ( brokenUsername.length > 0 && validAffiliations.includes(brokenUsername[0].toLowerCase()) ) {
    return logoAndUsername = '<div><img src="./images/logos/logo.' + brokenUsername[0].toLowerCase() + '.png"><span>' + brokenUsername[1] + '</span></div>';
  } else {
    return logoAndUsername = '<div><img src="./images/icons/profile.png"><span>' + username + '</span></div>';
  };
};

function updateUI() {
  switch ( page ) {
    case 'room':
      updateRoomUI();
      break;
    case 'poll':
      updatePollUI();
      break;
  };
};

function updateRoomUI() {

  // Escuchar para desconectar

  document.getElementById('button__disconnect').addEventListener('click', disconnectUser);
  document.getElementById('button__exit').addEventListener('click', exitRoom);
  document.getElementById('button__close_room').addEventListener('click', purgeRoom);

  console.log('Updating UI')

  var pageURL = new URL(window.location);
  var getRoomName = pageURL.searchParams.get('r');
  var getRoomPassword = pageURL.searchParams.get('p');

  if ( getRoomName ) { document.getElementById('input__room_name').value = getRoomName; };
  if ( getRoomPassword ) { document.getElementById('input__password').value = getRoomPassword; };
  
  var username = getCookie(cookieUsername);

  if ( username === null ) {

    document.getElementById('entrance').style.display = 'block';

  } else {

    document.getElementById('entrance').style.display = 'none';
    document.getElementById('lobby').style.display = 'block';

    setUserProfile(username);

    var roomName = getCookie(cookieRoomName);

    if ( !(roomName === null) ) {

      document.getElementById('room').style.display = 'block';
      document.getElementById('lobby').style.display = 'none';

      document.getElementById('information__room').style.display = 'block';
      document.getElementById('button__exit').style.display = 'inline';

      document.getElementById('text__room_name').innerHTML = roomName;

      var privateKey = getCookie(cookiePrivateKey);
      if ( privateKey === null ) {
        document.getElementById('text__password').innerHTML = getCookie(cookiePublicKey);
      } else {
        document.getElementById('text__password').innerHTML = getCookie(cookiePublicKey);
        document.getElementById('text__private_key').innerHTML = privateKey + '&nbsp;·&nbsp;';
        document.getElementById('button__close_room').style.display = 'inline';
      }

      document.getElementById('button__exit').addEventListener('click', exitRoom);
      document.getElementById('button__close_room').addEventListener('click', purgeRoom);
      updateTurnOrder();
      var updateRoom = setInterval(updateTurnOrder, turnRefreshTime);

    }

  }

}

function updatePollUI() {

  document.getElementById('button__disconnect').addEventListener('click', disconnectUser);

  var username = getCookie(cookieUsername);

  if ( username === null ) {
    document.getElementById('entrance').style.display = 'block';
    return;
  }

  document.getElementById('entrance').style.display = 'none';
  setUserProfile(username);

  var pageURL = new URL(window.location);
  var pollID = pageURL.searchParams.get('id');

  if ( pollID === null ) {
    document.getElementById('poll__create').style.display = 'block';
    document.getElementById('button__create_poll').addEventListener('click', createPoll);
    return;
  };

  document.getElementById('poll__create').style.display = 'none';

  var pollData = {};

  fetch('/api/polls/' + pollID,
    {
      method: 'GET',
      headers: 
      {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin'
    })
  .then(function(res) {
    switch ( res.status ) {

      case 200:
        res.json()
        .then(function(data) {

          if ( data.votes === undefined ) {
            document.getElementById('poll__vote').style.display = 'block';
            pollData = data;
            buildPoll(pollData);
            setTimeout(function(){location.reload();}, pollData.duration - (Date.now() - pollData.timestamp));

          } else {
            document.getElementById('poll__results').style.display = 'block';
            pollData = data;
            showResults(pollData);

          };

        });
        break;

      case 404:
        alert('No se ha encontrado la encuesta');
        break;
    };
  });
};

/*
 *
 *   Fetch options templates
 *
 */

var fetchGetOptions = {
  method: 'GET',
  headers: 
  {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json'
  },
  credentials: 'same-origin'
};

function fetchPostOptions(requestBody) {
  return {
    'method': 'POST',
    'headers': 
    {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    'credentials': 'same-origin',
    'body': requestBody
  };
};

function fetchPutOptions(requestBody) {
  return {
    'method': 'PUT',
    'headers': 
    {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    'credentials': 'same-origin',
    'body': requestBody
  };
};

function fetchDeleteOptions(requestBody) {
  return {
    'method': 'DELETE',
    'headers': 
    {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    'credentials': 'same-origin',
    'body': requestBody
  };
};

function fetchOptions(method, body = false) {
  let options = {
    'method': method,
    'headers': 
    {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    'credentials': 'same-origin'
  };
  if ( body ) { options.body = JSON.stringify(body) };
  return options;
}

/*
 *
 *   Turn order functions
 *
 */

function addUserIntervention(user, order) {
  var brokenUsername = user.username.split('_');
  if ( brokenUsername.length > 0 && validAffiliations.includes(brokenUsername[0].toLowerCase()) ) {
    var logoAndUsername = '<div><img src="./images/logos/logo.' + brokenUsername[0].toLowerCase() + '.png"><span>' + brokenUsername[1] + '</span>';
  } else {
    var logoAndUsername = '<div><img src="./images/icons/profile.png"><span>' + user.username + '</span>';
  };

  if ( user.userID === undefined ) {
    return '<div>' + logoAndUsername + '</div>';

  } else {
    var buttonMicrophone = '<a onclick="giveTurn(\'' + order + '\',\'' + user.userID +'\');">' + iconMicrophone + '</a>';
    var buttonTrash = '<a onclick="removeTurn(\'' + order + '\',\'' + user.userID +'\');">' + iconTrash + '</a>';
    return '<div>' + logoAndUsername + buttonMicrophone + buttonTrash + '</div>';
  }
};

function updateTurnOrder() {
  console.log('Updating turn order')
  fetch('/api/state', fetchGetOptions)
  .then(res=>res.json())
  .then( function(turnOrder) {
    for (var order in turnOrder) {
      if ( turnOrder[order].length > 0 ) {
        document.getElementById('room__order__' + order).innerHTML = '<h2>' + turnNames[order] + '</h2>' + turnOrder[order].map(x => addUserIntervention(x, order)).join('');
      } else {
        document.getElementById('room__order__' + order).innerHTML = '';
      };
    };
  });
};

function switchRoomPolicy() {

  var roomName = getCookie(cookieRoomName),
      privateKey = getCookie(cookiePrivateKey);

  var requestBody = JSON.stringify({
    roomName: roomName,
    privateKey: privateKey
  });

  fetch('/api/switch-room-policy', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        updateRoomPolicy();
        break;

      case 401:
        alert('Contraseña incorrecta');
        break;

      case 404:
        alert('No se ha encontrado la sala');
        break;
    };
  }).catch(e => console.log(e))
};

/*
 *
 *   Customize enter behaviour
 *
 */

function processEnter(e, buttonId) {
  if (null == e)
      e = window.event;
  if (e.keyCode == 13)  {
      document.getElementById(buttonId).click();
      return false;
  }
};

// Listen for the username

document.getElementById('button__set_username').addEventListener('click', function () {

  console.log('Registering with username')

  var username = document.getElementById('input__username').value;
  setCookie(cookieUsername, username, cookieLife);

  var userID = (Math.random().toString(36)+'000000').slice(2, 8)
  setCookie(cookieUserID, userID, cookieLife)

  updateUI();

});

// Listen to create a room

document.getElementById('button__create_room').addEventListener('click', function () {

  var roomName = document.getElementById('input__new_room_name').value,
      publicKey = (Math.random().toString(10)+'000000').slice(2, 8),
      privateKey = (Math.random().toString(10)+'000000').slice(2, 8),
      username = getCookie(cookieUsername),
      userID = getCookie(cookieUserID);

  var requestBody = JSON.stringify({
    username: username, 
    userID: userID, 
    roomName: roomName, 
    publicKey: publicKey, 
    privateKey: privateKey,
    subscription: pushSubscription
  });

  fetch('/api/create', fetchPostOptions(requestBody))
  .then( function(res) {

    if ( res.status === 200 ) {
      setCookie(cookieRoomName, roomName, cookieLife);
      setCookie(cookiePublicKey, publicKey, cookieLife);
      setCookie(cookiePrivateKey, privateKey, cookieLife);
      updateUI();
    } else if ( res.status === 409 ) {
      alert('Ya existe una sala con ese nombre')
    };
  })
  .catch(e => console.log(e));
});

// Escuchar para unirse a una sala

document.getElementById('button__join_room').addEventListener('click', function () {

  var roomName = document.getElementById('input__room_name').value,
      password = document.getElementById('input__password').value,
      username = getCookie(cookieUsername),
      userID = getCookie(cookieUserID);

  var requestBody = JSON.stringify({
    username: username, 
    userID: userID, 
    roomName: roomName, 
    password: password,
    subscription: pushSubscription
  });

  fetch('/api/join', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        res.json()
        .then( function(data) {
          setCookie(cookieRoomName, roomName, cookieLife);
          setCookie(cookiePublicKey, data.publicKey, cookieLife);
          if ( !(data.privateKey === undefined) ) { setCookie(cookiePrivateKey, data.privateKey, cookieLife) };
          updateUI();
        })
        .catch(e => console.log(e));
        break;

      case 401:
        alert('Contraseña incorrecta');
        break;

      case 404:
        alert('No se ha encontrado la sala');
        break;
    };
  });
});

//

function createRoom(roomName, publicKey, privateKey, username, userID, subscription) {

  var requestBody = JSON.stringify({
    roomName: roomName,
    publicKey: publicKey,
    privateKey: privateKey
  });

  fetch('/api/rooms', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        setCookie(cookieRoomName, roomName, cookieLife);
        setCookie(cookiePublicKey, publicKey, cookieLife);
        setCookie(cookiePrivateKey, privateKey, cookieLife);
        updateUI();
        break;

      case 409:
        alert('Ya existe una sala con ese nombre');
        break;
    };
  })
  .catch(e => console.log(e));
};

function joinRoom(roomName, password, username, userID, subscription) {

  var requestBody = JSON.stringify({
    roomName: roomName,
    publicKey: publicKey,
    username: username,
    userID: userID,
    subscription: subscription
  });

  fetch('/api/rooms/' + roomName + '/users', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        setCookie(cookieRoomName, roomName, cookieLife);
        res.json()
        .then( function(data) {
          setCookie(cookiePublicKey, data.publicKey, cookieLife);
          if ( !(data.privateKey === undefined) ) { setCookie(cookiePrivateKey, data.privateKey, cookieLife) };
        });
        updateUI();
        break;

      case 401:
        alert('Contraseña incorrecta');
        break;

      case 404:
        alert('No se ha encontrado la sala');
        break;
    };
  }).catch(e => console.log(e));

};

// Función para pedir turno

function askTurn(turnPriority) {

  var username = getCookie(cookieUsername),
      userID = getCookie(cookieUserID),
      roomName = getCookie(cookieRoomName),
      publicKey = getCookie(cookiePublicKey);

  var requestBody = JSON.stringify({
    username: username, 
    userID: userID, 
    roomName: roomName, 
    publicKey: publicKey,
    turnPriority: turnPriority
  });

  fetch('/api/ask', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        updateTurnOrder();
        break;

      case 400:
        alert('Error en la solicitud');
        break;

      case 401:
        alert('Contraseña incorrecta');
        break;

      case 403:
        alert('No se admiten más preguntas');
        break;

      case 404:
        alert('No se ha encontrado la sala');
        break;
    };
  });
};

// Función para otorgar turno

function giveTurn(turnPriority, userID = getCookie(cookieUserID)) {

  var roomName = getCookie(cookieRoomName),
      privateKey = getCookie(cookiePrivateKey);

  var requestBody = JSON.stringify({
    roomName: roomName,
    publicKey: publicKey,
    userID: userID,
    turnPriority: turnPriority
  });

  fetch('/api/speak', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        updateTurnOrder();
        break;

      case 400:
        alert('Error en la solicitud.');
        break;

      case 401:
        alert('Contraseña incorrecta.');
        break;

      case 403:
        alert('No se admiten más preguntas.');
        break;

      case 404:
        alert('No se ha encontrado la sala.');
        break;
    };
  });
};

// Función para cancelar turno

function removeTurn(turnPriority, userID = getCookie(cookieUserID)) {

  var username = getCookie(cookieUsername),
      roomName = getCookie(cookieRoomName),
      publicKey = getCookie(cookiePublicKey);

  var requestBody = JSON.stringify({
    username: username,
    userID: userID,
    roomName: roomName,
    publicKey: publicKey,
    turnPriority: turnPriority
  });

  fetch('/api/shut', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        updateTurnOrder();
        break;

      case 400:
        alert('Error en la solicitud');
        break;

      case 401:
        alert('Contraseña incorrecta');
        break;

      case 404:
        alert('No se ha encontrado la sala');
        break;
    };
  });
};

//

document.getElementById('button__change_policy').addEventListener('click', switchRoomPolicy);

// Función para purgar todos los datos y volver al inicio

function disconnectUser() {
  console.log('Disconnecting user');
  let userCookies = [cookieUsername, cookieUserID, cookieRoomName, cookiePublicKey, cookiePrivateKey];
  userCookies.map(eraseCookie);

  location.reload();
};

function exitRoom() {
  console.log('Disconnecting from room');
  let roomCookies = [cookieRoomName, cookiePublicKey, cookiePrivateKey];
  roomCookies.map(eraseCookie);

  location.reload();
};

function purgeRoom() {

  console.log('Destroying the room')

  var roomName = getCookie(cookieRoomName),
      privateKey = getCookie(cookiePrivateKey);

  var requestBody = JSON.stringify({
    roomName: roomName,
    privateKey: privateKey
  })

  fetch('/api/delete', fetchPostOptions(requestBody))
  .then( function(res) {

    switch ( res.status ) {
      case 200:
        console.log('Room successfully destroyed from the server')
        let roomCookies = [cookieRoomName, cookiePublicKey, cookiePrivateKey];
        roomCookies.map(eraseCookie);

        location.reload();
        break;

      case 403:
        console.log('Wrong room password');
        break;
    };
  });
};

/*
 *
 *   Poll helper functions
 *
 */

function buildPoll(pollData) {

  document.getElementById('header__poll_title').innerHTML = pollData.name;
  document.getElementById('text__poll_description').innerHTML = pollData.description;

  let creationDate = new Date(pollData.timestamp);
  let creationDay = creationDate.toLocaleString('es-es', {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'});
  let creationTime = creationDate.toLocaleString('es-es', {hour:'numeric', minute:'numeric'});
  var dayTimeConnector = ['01', '13'].includes(creationTime.slice(0,2)) ? ' a la ' : ' a las ';
  document.getElementById('text__creation_time').innerHTML = 'Creada el ' + creationDay + dayTimeConnector + creationTime;

  let choiceOptions = '';

  for (var i=0; i<pollData.choices.length; i++) {
    let choice = pollData.choices[i]
    choiceOptions += '<form class="poll__choice"><h2>' + choice.question + '</h2>';
    for (var j=0; j<choice.answers.length; j++) {
      choiceOptions += '<div class="poll__choice--' + i + '"><input type="radio" name="input__' + i + '" id="input__' + i + '_' + j + '" value="' + choice.answers[j] + '">';
      choiceOptions += '<label for="input__' + i + '_' + j + '">' + choice.answers[j] + '</label></div>';
    };
    choiceOptions += '</form>';
  };

  choiceOptions += '<div class="form__buttons"><button type="button" id="button__cast_ballot" class="boton__primario">Votar</button></div>';

  document.getElementById('poll__content').insertAdjacentHTML('beforeend', choiceOptions);

  document.getElementById('button__cast_ballot').addEventListener('click', castVote);

};

function castVote() {

  let pageURL = new URL(window.location);
  let pollID = pageURL.searchParams.get('id');

  let votes = [];
  let choices = document.getElementsByClassName('poll__choice');

  for (var i=0; i<choices.length; i++) {
    let answers = document.getElementsByClassName('poll__choice--' + i);
    for (var j=0; j<answers.length; j++) {
      let answer = document.getElementById('input__' + i + '_' + j);
      if (document.getElementById('input__' + i + '_' + j).checked) { votes.push( answer.value ); break; };
    };
  };

  fetch('/api/polls/' + pollID, fetchOptions('POST', {"votes": votes}))
  .then(function(res) {
    switch ( res.status ) {

      case 200:
        res.json()
        .then(data => alert('Voto emitido con certificado ' + data.certificate));
        break;

      case 400:
        alert('Error en la emisión del voto');
        break;

      case 401:
        alert('No se admiten más votos');
        break;

      case 403:
        alert('El usuario no tiene acceso a la votación');
        break;

      case 404:
        alert('No se ha encontrado la votación');
        break;
    };
  });
};

function createNewQuestion() {
  let n = document.getElementsByClassName('poll__question').length;

  let newQuestion = `
    <div class="poll__question" id="poll__question--${n}">
      <label for="input__question--${n}">
        <p>Pregunta #${n+1}</p>
        <a class="helper helper--disconnect" onclick="document.getElementById('poll__question--${n}').remove()">Eliminar</a>
      </label>
      <input type="text" id="input__question--${n}" name="input__question--${n}" placeholder="P ?? R1 ; R2 ; ... ; R1" onkeydown="">
    </div>`;

  document.getElementById('poll__questions').insertAdjacentHTML('beforeend', newQuestion);

  return newQuestion;
};

function createPollOptions() {

  let name = document.getElementById('input__poll_name').value;
  let description = document.getElementById('input__poll_description').value;
  let type = document.getElementById('select__poll_type').value;
  let duration = document.getElementById('select__poll_duration').value;

  let choices = [];
  let nquestions = document.getElementsByClassName('poll__question').length;

  for (var i=0; i<nquestions; i++) {
    let questionText = document.getElementById('input__question--' + i).value;
    if ( questionText.includes('??') ) {
      let [question, allAnswers] = questionText.split('??').map(x => x.trim());
      let answers = allAnswers.split(';').map(x => x.trim());
      choices.push({"question": question, "answers": answers})

    } else {
      alert('Formato incorrecto en la pregunta #' + i);
      return false;
    }
  }

  return {
    "name": name,
    "description": description,
    "type": type,
    "duration": duration,
    "choices": choices,
    "validVoters": []
  };

};

function createPoll() {

  options = createPollOptions();

  if ( options ) {

    fetch('/api/polls', fetchOptions('PUT', {"options": options}))
    .then(function(res) {

      switch ( res.status ) {

        case 200:
          res.json()
          .then(function(data) {
            console.log(data);
            window.location.href = './vote?id=' + data.pollID;
          });
          break;

        case 409:
          alert('Error en la creación de la votación');
          break;

        default:
          alert('Error no identificado');
          break;

      };
    });

  } else {
    alert('Error');
    return false;
  };

};

function showResults(pollData) {

  let choices = pollData.choices,
      votes = pollData.votes.map(x => x.votes),
      voters = pollData.votes.map(x => x.username),
      certificates = pollData.votes.map(x => x.certificate);

  let n = votes.length,
      thisPoll = '';

  document.getElementById('header__poll_results_title').innerHTML = pollData.name;
  document.getElementById('text__poll_results_description').innerHTML = pollData.description;

  let endDate = new Date(pollData.timestamp + pollData.duration);
  let endDay = endDate.toLocaleString('es-es', {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'});
  let endTime = endDate.toLocaleString('es-es', {hour:'numeric', minute:'numeric'});
  var dayTimeConnector = ['01', '13'].includes(endTime.slice(0,2)) ? ' a la ' : ' a las ';
  document.getElementById('text__poll_end_time').innerHTML = 'Finalizó el ' + endDay + dayTimeConnector + endTime;

  for (var i=0; i<choices.length; i++) {
    let allAnswers = choices[i].answers;
    let thisQuestion = '<div class="poll__result"><h2>' + choices[i].question + '</h2>';
    for (var j=0; j<allAnswers.length; j++) {
      let answer = allAnswers[j];
      let supporters = []
      votes.forEach(function(vote, index) {
        if ( vote[i] === answer ) { supporters.push(voters[index]); };
      });
      let percent = Math.floor(100*(supporters.length)/n);
      let thisAnswer = '<div><p>' + answer + '&nbsp;<i class="helper">' + (supporters.length) + '/' + n.toString() + ', ' + percent.toString() + '%</i></p>'
      thisAnswer += '<div class="poll__bar"><div style="width: ' + percent.toString() + '%"></div></div>';
      thisAnswer += '<div class="poll__supporters">' + supporters.map(printUserProfile).join('') + '</div>';
      thisAnswer += '</div>'
      thisQuestion += thisAnswer;
    }
    thisQuestion += '</div>';
    thisPoll += thisQuestion;
  }

  document.getElementById('poll__results--answers').innerHTML = thisPoll;

};
