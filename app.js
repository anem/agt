const express = require('express');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const spdy = require('spdy');
const fs = require('fs');
const webpush = require('web-push');
const path = require('path');
const crypto = require('crypto');

var app = express();

// Puerto desde el que escuchará la aplicación
const PORT = process.env.PORT || 3000;
const roomDirectory = path.join('private', 'rooms');
const roomLife = 1000 * 60 * 60 * 24; // Un día en milisegundos
const pollDirectory = path.join('private', 'polls');
const turnPriorityValues = ['i', 'ii', 'iii', 'iv'];
const REQ = 'REQ';

function log(args) {
	var offset = new Date().getTimezoneOffset();
	var now = new Date(Date.now()-offset*60*1000).toISOString().replace('T',' ').substr(0,23);
	return now + ' : ' + args;
};

function API_log(method, type, text) {
	return log('API/' + method.padEnd(8) + '(' + String(type) + ') : ' + text);
}

// SSL certificate options

const options = {
  key: fs.readFileSync(path.join(__dirname, 'private', 'server.key')),
  cert: fs.readFileSync(path.join(__dirname, 'private', 'server.crt')),
  spdy: { protocols: ['h2', 'http/1.1' ] }
}

// Define app keys for the Push service

if ( fs.existsSync(path.join(__dirname, 'private', 'keyring.json')) ) {
	let keyring = JSON.parse(fs.readFileSync(path.join(__dirname, 'private', 'keyring.json')));
	if ( ! keyring.GCMServerKey === null ) {
		var privateGCMServerKey = keyring.GCMServerKey;
		webpush.setGCMAPIKey(privateGCMServerKey);
	}
	var vapidPublicKey = keyring.VAPIDKeys.publicKey;
	var vapidPrivateKey = keyring.VAPIDKeys.privateKey;
} else {
	var vapidKeys = webpush.generateVAPIDKeys();
	var vapidPublicKey = vapidKeys.publicKey;
	var vapidPrivateKey = vapidKeys.privateKey;
	
	fs.writeFileSync(path.join(__dirname, 'private', 'keyring.json'), JSON.stringify({"VAPIDKeys":vapidKeys}));
};

webpush.setVapidDetails(
	'mailto:tic@anem.es',
	vapidPublicKey,
	vapidPrivateKey
);

/*
 *
 *  Funciones útiles
 *
 */

function createRoom(roomName, publicKey, privateKey, username, userID, subscription) {
	let room = {
		"info": {
			"name": roomName,
			"created": Date.now(),
			"password" : {
				"public": publicKey,
				"private": privateKey
			},
			"open": true
		},
		"users": [
			{
				"username": username,
				"userID": userID,
				"is_admin": true,
				"subscription": subscription
			}
		],
		"turns": {
			"i":   [],
			"ii":  [],
			"iii": [],
			"iv" : []
		}
	};

	let room_json = JSON.stringify(room);
	fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);
}

function deleteRoom(roomName, privateKey) {

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( roomData.info.password.private === privateKey ) {
			fs.unlinkSync(path.join(__dirname, roomDirectory, roomName + '.json'));
			return 200;

		} else {
			return 403;
		}
	} else {
		return 404;
	}
};

function addUser(roomName, password, username, userID, subscription) {

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( roomData.users.map(user => user.userID).includes(userID) ) {

			var index = roomData.users.map(user => user.userID).indexOf(userID);

			if ( roomData.users[index].is_admin || password === roomData.info.password.public ) {
				return 200;
			} else if ( password === roomData.info.password.private ) {

				roomData.users[index].is_admin = true;
				let room_json = JSON.stringify(roomData);
				fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);

				// Return an object consisting of the keys
				return {"publicKey": roomData.info.password.public, "privateKey": roomData.info.password.private};
			}
		};

		if ( password === roomData.info.password.private ) {

			// User is logged in as an admin
			roomData.users.push({
				"username": username,
				"userID": userID,
				"is_admin": true,
				"subscription": subscription
			});

			let room_json = JSON.stringify(roomData);
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);

			// Return an object consisting of the keys
			return {"publicKey": roomData.info.password.public, "privateKey": roomData.info.password.private};

		} else if ( password === roomData.info.password.public ) {

			// User is logged in as a normal user
			roomData.users.push({
				"username": username,
				"userID": userID,
				"is_admin": false,
				"subscription": subscription
			});

			let room_json = JSON.stringify(roomData);
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);

			// Return a success error
			return {"publicKey": password};

		} else {
			// If the password doesn't match, throw a 401 Unauthorized error
			return 401;
		};
	} else {
		// If the file does not exist, throw a 404 Not Found error
		return 404;
	};
};

function addTurn(roomName, password, username, userID, turnPriority) {

	if ( turnPriorityValues.includes(turnPriority) ) {
		if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
			let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
			let roomData = JSON.parse(roomDataJSON);

			if ( Object.values(roomData.info.password).includes(password) ) {
				if ( roomData.info.open ) {
					if ( !roomData.turns[turnPriority].map(x=>x.userID).includes(userID) ) {

						roomData.turns[turnPriority].push({"username": username, "userID": userID, "time": Date.now()});
						
						let room_json = JSON.stringify(roomData);
						fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);
					}
					return 200;

				} else {
					return 403;
				};
			} else {
				return 401;
			};
		} else {
			return 404;
		};
	} else {
		return 400;
	};
};

function deleteTurn(roomName, password, userID, turnPriority) {

	if ( turnPriorityValues.includes(turnPriority) ) {
		if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
			let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
			let roomData = JSON.parse(roomDataJSON);
			console.log(password)
			console.log(Object.values(roomData.info.password))

			if ( Object.values(roomData.info.password).includes(password) ) {

				let index = roomData.turns[turnPriority].map(x=>x.userID).indexOf(userID);

				if ( index > -1 ) {
					roomData.turns[turnPriority].splice(index, 1);
					let room_json = JSON.stringify(roomData);
					fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), room_json);

					return 200;
				} else {
					return 400;
				};
			} else {
				return 401;
			};
		} else {
			return 404;
		};
	} else {
		return 400;
	};
};

function getPushInformation (roomName, privateKey, userID) {
	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);
		if ( roomData.info.password.private === privateKey ) {

			let user = roomData.users.find(x => x.userID === userID);

			if ( user === undefined ) {
				return 400
			} else if ( user.subscription === null ) {
				return 400
			} else {
				return user.subscription;
			};

		} else {
			return 401;
		}
	} else {
		return 404;
	};
};

/*
 *
 *  Configuración de la aplicación
 *
 */

 const shouldCompress = (req, res) => {
  // don't compress responses asking explicitly not
  if (req.headers['x-no-compression']) {
    return false
  }

  // use compression filter function
  return compression.filter(req, res)
}

app.use(compression({ filter: shouldCompress }));

app.use(cookieParser());

// Configura la posibilidad de responder a solicitudes con URL y JSON
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Carga los archivos desde la carpeta estáticos
app.use(express.static(path.join(__dirname, 'public'), {
	index: 'index.html',
	extensions: ['html'], // Permite no escribir la extensión de los archivos .html
}));

// Crea una nueva sala
app.post('/api/create', function(req, res, next) {
	
	var username = req.body.username;
	var userID = req.body.userID;
	var subscription = req.body.subscription;
	var roomName = req.body.roomName;
	var publicKey = req.body.publicKey;
	var privateKey = req.body.privateKey;

	console.log(API_log('create', REQ, 'user: ' + userID + ' creating room: ' + roomName));

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);
		
		if ( Date.now() - roomData.info.created > roomLife ) {

			console.log(API_log('create', 200, 'overwriting room: ' + roomName));
			createRoom(roomName, publicKey, privateKey, username, userID, subscription);

			res.status(200).end();

		} else {
			console.log(API_log('create', 409, 'room: ' + roomName + ' already exists'));
			res.status(409).end();
		};
	} else {

		console.log(API_log('create', 200, 'creating room: ' + roomName));
		createRoom(roomName, publicKey, privateKey, username, userID);

		res.status(200).end();
	};

	next();

});

// Delete a room
app.delete('/api/delete', function(req, res, next) {

	var roomName = req.body.roomName;
	var privateKey = req.body.privateKey;

	console.log(API_log('delete', REQ, 'room: ' + roomName + ' being deleted by admin'));

	let answer = deleteRoom(roomName, privateKey);

	if ( answer === 200) {
		console.log(API_log('delete', 200, 'deleted room: ' + roomName));
		res.status(200).end();

	} else if ( answer === 403) {
		console.log(API_log('delete', 403, 'wrong password to delete room: ' + roomName));
		res.status(403).end();

	} else if ( answer === 404) {
		console.log(API_log('delete', 404, 'room: ' + roomName + ' not found'));
		res.status(404).end();

	} else {
		res.status(500).end();
	};

	next();

});

// Únete a una nueva sala
app.post('/api/join', function(req, res, next) {

	var username = req.body.username;
	var userID = req.body.userID;
	var roomName = req.body.roomName;
	var password = req.body.password;
	var subscription = req.body.subscription;

	console.log(API_log('join', REQ, 'user: ' + userID + ' joining room: ' + roomName));
	
	let answer = addUser(roomName, password, username, userID, subscription);

	if ( typeof answer === 'object' && !(answer === null)  ) {
		console.log(API_log('join', 200, 'user: ' + userID + ' joined room: ' + roomName));
		res.status(200).send(answer).end();

	} else if ( answer === 401 ) {
		console.log(API_log('join', 401, 'wrong password from: ' + userID + ' for room: ' + roomName));
		res.status(401).end();

	} else if ( answer === 404 ) {
		console.log(API_log('join', 404, 'room: ' + roomName + ' not found'));
		res.status(404).end();
	};

	next();

});

// Ask to be included in a room's turn order

app.post('/api/ask', function(req, res, next) {

	var username = req.body.username;
	var userID = req.body.userID;
	var roomName = req.body.roomName;
	var publicKey = req.body.publicKey;
	var turnPriority = req.body.turnPriority;

	console.log(API_log('ask', REQ, 'user ' + userID + ' asking at room ' + roomName + ' with priority ' + turnPriority));

	let answer = addTurn(roomName, publicKey, username, userID, turnPriority);

	if ( answer === 200 ) {
		console.log(API_log('ask', 200, 'question by ' + userID + ' in room ' + roomName + ' accepted'));
		res.status(200).end();

	} else if ( answer === 400 ) {
		console.log(API_log('ask', 400, 'wrong turn priority by ' + userID + ' in room ' + roomName));
		res.status(400).end();

	} else if ( answer === 401 ) {
		console.log(API_log('ask', 401, 'wrong password from ' + userID + ' for room ' + roomName));
		res.status(401).end();

	} else if ( answer === 403 ) {
		console.log(API_log('ask', 403, 'question by ' + userID + ' in room ' + roomName + 'correct, but room is closed'));
		res.status(403).end();

	} else if ( answer === 404 ) {
		console.log(API_log('ask', 404, 'room ' + roomName + ' requested by ' + userID + ' not found'));
		res.status(404).end();
	};

	next();

});

// Ask to be included in a room's turn order

app.post('/api/speak', function(req, res, next) {

	var userID = req.body.userID;
	var roomName = req.body.roomName;
	var privateKey = req.body.privateKey;
	var turnPriority = req.body.turnPriority;

	console.log(API_log('speak', REQ, 'giving turn to: ' + userID + ' in room: ' + roomName + ' with priority: ' + turnPriority));

	let answer = deleteTurn(roomName, privateKey, userID, turnPriority);

	if ( answer === 200 ) {
		console.log(API_log('speak', 200, 'turn by ' + userID + ' in room ' + roomName + ' given'));

		let subscription = getPushInformation(roomName, privateKey, userID);

		if ( typeof subscription === 'object' && !(subscription === null) ) {
			webpush.sendNotification(subscription, 'Turno de palabra');
		}

		res.status(200).end();

	} else if ( answer === 400 ) {
		console.log(API_log('speak', 400, 'wrong request by ' + userID + ' in room ' + roomName));
		res.status(400).end();

	} else if ( answer === 401 ) {
		console.log(API_log('speak', 401, 'wrong password from ' + userID + ' for room ' + roomName));
		res.status(401).end();

	} else if ( answer === 404 ) {
		console.log(API_log('speak', 404, 'room ' + roomName + ' requested by ' + userID + ' not found'));
		res.status(404).end();
	};

	next();

});

// Ask to be included in a room's turn order

app.post('/api/shut', function(req, res, next) {

	var username = req.body.username;
	var userID = req.body.userID;
	var roomName = req.body.roomName;
	var publicKey = req.body.publicKey;
	var turnPriority = req.body.turnPriority;

	console.log(API_log('shut', REQ, 'deleting turn from ' + userID + ' in room ' + roomName + ' with priority ' + turnPriority));

	let answer = deleteTurn(roomName, publicKey, userID, turnPriority);

	if ( answer === 200 ) {
		console.log(API_log('shut', 200, 'turn by ' + userID + ' in room ' + roomName + ' deleted'));
		res.status(200).end();

	} else if ( answer === 400 ) {
		console.log(API_log('shut', 400, 'wrong request by ' + userID + ' in room ' + roomName));
		res.status(400).end();

	} else if ( answer === 401 ) {
		console.log(API_log('shut', 401, 'wrong password from ' + userID + ' for room ' + roomName));
		res.status(401).end();

	} else if ( answer === 404 ) {
		console.log(API_log('shut', 404, 'room ' + roomName + ' requested by ' + userID + ' not found'));
		res.status(404).end();
	};

	next();

});

// Close the room to further intervention

app.post('/api/switch-room-policy', function(req, res, next) {

	var roomName = req.body.roomName,
	    privateKey = req.body.privateKey;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( privateKey === roomData.info.password.private ) {
			roomData.info.open = roomData.info.open ? false : true;
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), JSON.stringify(roomData));
			res.status(200).end();
		
		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

// Get the current turn order of a room

app.get('/api/state', function(req, res, next) {

	var roomName = req.cookies['anem_agtmod_roomName'],
	    publicKey = req.cookies['anem_agtmod_publicKey'],
	    privateKey = req.cookies['anem_agtmod_privateKey'];

	var password = privateKey === undefined ? publicKey : privateKey;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( password === roomData.info.password.private ) {
			res.status(200).send(roomData.turns).end();

		} else if ( password === roomData.info.password.public ) {
			var turns = roomData.turns;
			Object.keys(turns).map(t => turns[t] = turns[t].map(function(x){return{username:x.username};}));
			res.status(200).send(turns).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});


/*
 *
 *   /api/keys
 *
 */

app.get('/api/keys', function(req, res, next) {

	res.status(200).send({"key": vapidPublicKey}).end();

	next();

});

/*
 *
 *   /api/rooms
 *
 */

app.put('/api/rooms', function(req, res, next) {

	var roomName = req.body.roomName,
	    publicKey = req.body.publicKey,
	    privateKey = req.body.privateKey;

	if ( !(fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json'))) ) {

		let roomData = {
			"info": {
				"name": roomName,
				"created": Date.now(),
				"keyring": {
					"publicKey": publicKey,
					"privateKey": privateKey
				},
				"open": true
			},
			"users": [],
			"turns": {
				"i": [],
				"ii": [],
				"iii": [],
				"iv": []
			}
		};
	
		let roomDataJSON = JSON.stringify(roomData);
		fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);

		res.status(200).end();

	} else {
		res.status(409).end();
	};

	next();

});

app.delete('/api/rooms', function(req, res, next) {

	fs.readdirSync(path.join(__dirname, roomDirectory)).forEach( function(room) {

		if ( !(fs.statSync(path.join(__dirname, roomDirectory, room)).isDirectory()) ) {

			let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, room));
			let roomData = JSON.parse(roomDataJSON);

			if ( Date.now() - roomData.info.created > roomLife ) {
				fs.unlinkSync(path.join(__dirname, roomDirectory, room));
			};
		};
	});

	res.status(200).end();

	next();

});

/*
 *
 *   /api/rooms/:roomName
 *
 */

app.get('/api/rooms/:roomName', function(req, res, next) {

	var roomName = req.params.roomName,
	    publicKey = req.cookies.anem_agtmod_publicKey,
	    privateKey = req.cookies.anem_agtmod_privateKey;

	var password = privateKey === undefined ? publicKey : privateKey;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( Object.values(roomData.info.keyring).includes(password) ) {

			let roomUsers = roomData.users.map(user => user.username);
			let roomTurns = roomData.turns;

			if ( password === roomData.info.keyring.publicKey ) {
				Object.keys(roomTurns).map(t => roomTurns[t] = roomTurns[t].map(function(x){return{username:x.username};}));
			};

			let roomGetData = {
				"users": roomUsers,
				"policy": roomData.info.open,
				"turns": roomTurns
			}

			res.status(200).send(roomGetData).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

app.post('/api/rooms/:roomName', function(req, res, next) {

	var roomName = req.params.roomName,
			privateKey = req.body.privateKey,
			options = req.body.options;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( privateKey === roomData.info.keyring.privateKey ) {

			if ( options.switchRoomPolicy ) { 
				roomData.info.open = roomData.info.open ? false : true;
			}	

			let roomDataJSON = JSON.stringify(roomData);
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);

			res.status(200).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

app.delete('/api/rooms/:roomName', function(req, res, next) {

	var roomName = req.params.roomName,
			privateKey = req.body.privateKey;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( (privateKey === roomData.info.keyring.privateKey) ) {
			fs.unlinkSync(path.join(__dirname, roomDirectory, roomName + '.json'));
			res.status(200).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

/*
 *
 *   /api/rooms/:roomName/users
 *
 */

const cookieUsername = 'anem_agtmod_username';
const cookieUserID = 'anem_agtmod_userID';

app.put('/api/rooms/:roomName/users', function(req, res, next) {

	var roomName = req.params.roomName,
			password = req.body.password,
	    username = req.cookies[cookieUsername],
	    userID = req.cookies[cookieUserID],
	    subscription = req.body.subscription;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( Object.values(roomData.info.keyring).includes(password) ) {

			if ( !(roomData.users.map(user => user.userID).includes(userID)) ) {
				roomData.users.push({
					"username": username,
					"userID": userID,
					"subscription": subscription
				});

				let roomDataJSON = JSON.stringify(roomData);
				fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);
			};

			if ( roomData.info.keyring.publicKey === password ) {
				res.status(200).send({"publicKey": password});

			} else {
				res.status(200).send(roomData.info.keyring);
			};
		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

app.get('/api/rooms/:roomName/:password', function(req, res, next) {

	var roomName = req.params.roomName,
			password = req.params.password;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( password === roomData.info.keyring.publicKey ) {

			let users = roomData.users.map(function(x){return{"username":x.username};});
			res.status(200).send(users).end();

		} else if ( password === roomData.info.keyring.privateKey ) {

			let users = roomData.users.map(function(x){return{"username":x.username,"userID":x.userID};});
			res.status(200).send(users).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

/*
 *
 *   /api/rooms/:roomName/users/:userID
 *
 */

app.post('/api/rooms/:roomName/users/:userID', function(req, res, next) {

	var roomName = req.params.roomName,
	    userID = req.params.userID,
	    text = req.body.text;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {
		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		let user = roomData.users.find(x => x.userID === userID);

		if ( !(user === undefined) ) {

			webpush.sendNotification(user.subscription, text);
			res.status(200).end();

		} else {
			res.status(404).end();
		};
	} else {
		res.status(404).end();
	}

  next();

});

app.delete('/api/rooms/:roomName/users/:userID', function(req, res, next) {

	var roomName = req.params.roomName,
	    userID = req.params.userID;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		let index = roomData.users.map(user => user.userID).indexOf(userID);

		if ( index > -1 ) {
			roomData.users.splice(index, 1);
			let roomDataJSON = JSON.stringify(roomData);
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);
			res.status(200).end();

		} else {
			res.status(404).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

/*
 *
 *   /api/rooms/:roomName/turns
 *
 */

app.put('/api/rooms/:roomName/turns', function(req, res, next) {

	var roomName = req.params.roomName,
			publicKey = req.body.publicKey,
			username = req.body.username,
			userID = req.body.userID,
			turnType = req.body.turnType;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		if ( publicKey === roomData.info.keyring.publicKey ) {

			let index = roomData.turns[turnType].map(x => x.userID).indexOf(userID);

			if ( roomData.info.open ) {

				if ( index > -1 ) { roomData.turns[turnType].splice(index, 1); };

				roomData.turns[turnType].push({
					"username": username,
					"userID": userID,
					"timestamp": Date.now()
				});

				let roomDataJSON = JSON.stringify(roomData);
				fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);
				res.status(200).end();

			} else {
				res.status(400).end();
			};

		} else {
			res.status(401).end();
		};

	} else {
		res.status(404).end();
	};

	next();

});

app.delete('/api/rooms/:roomName/turns', function(req, res, next) {

	var roomName = req.params.roomName,
			publicKey = req.body.publicKey,
			username = req.body.username,
			userID = req.body.userID,
			turnType = req.body.turnType;

	if ( fs.existsSync(path.join(__dirname, roomDirectory, roomName + '.json')) ) {

		let roomDataJSON = fs.readFileSync(path.join(__dirname, roomDirectory, roomName + '.json'));
		let roomData = JSON.parse(roomDataJSON);

		let index = roomData.turns[turnType].map(x => x.userID).indexOf(userID);

		if ( index > -1 ) {
			roomData.turns[turnType].splice(index, 1);
			let roomDataJSON = JSON.stringify(roomData);
			fs.writeFileSync(path.join(__dirname, roomDirectory, roomName + '.json'), roomDataJSON);
			res.status(200).end();

		} else {
			res.status(404).end();
		};

	} else {
		res.status(404).end();
	};

	next();

});

/*
 *
 *   Polls API
 *
 */

function checkPoll(pollID) {
	return fs.existsSync(path.join(__dirname, pollDirectory, pollID + '.json'));
}

function readPoll(pollID) {
	let pollDataJSON = fs.readFileSync(path.join(__dirname, pollDirectory, pollID + '.json'));
	return JSON.parse(pollDataJSON);
}

function savePoll(pollData) {
	let pollID = pollData.pollID;
	fs.writeFileSync(path.join(__dirname, pollDirectory, pollID + '.json'), JSON.stringify(pollData));
}

/*
 *
 *   /api/polls
 *
 */

app.put('/api/polls', function(req, res, next) {

 	var pollID = crypto.randomBytes(18).toString('hex'),
 			privateKey = (Math.random().toString(10)+'000000').slice(2, 8),
 			options = req.body.options;

 	if ( !checkPoll(pollID) ) {

 		pollData = {
 			name: options.name,
 			description: options.description,
 			timestamp: Date.now(),
 			pollID: pollID,
 			privateKey: privateKey,
 			duration: parseInt(options.duration),
 			type: options.type,
 			validVoters: options.validVoters,
 			choices: options.choices,
 			votes: []
 		};

 		savePoll(pollData);
 		res.status(200).send({pollID: pollID, privateKey: privateKey}).end();

 	} else {
 		res.status(409).end();
 	};

 	next();

});

/*
 *
 *   /api/polls/:pollID
 *
 */

app.get('/api/polls/:pollID', function(req, res, next) {

 	var pollID = req.params.pollID;

 	if ( checkPoll(pollID) ) {

 		let pollData = readPoll(pollID);
 		delete pollData.privateKey;
 		delete pollData.validVoters;
 		if ( Date.now() - parseInt(pollData.timestamp) < parseInt(pollData.duration) ) { delete pollData.votes; console.log('Votes deleted'); };
 		res.status(200).send(pollData).end();

 	} else {
 		res.status(404).end();
 	};

 	next();

});

app.post('/api/polls/:pollID', function(req, res, next) {

	var pollID = req.params.pollID,
			username = req.cookies[cookieUsername],
			userID = req.cookies[cookieUserID],
			votes = req.body.votes;

	if ( checkPoll(pollID) ) {

		let pollData = readPoll(pollID);

		if ( Date.now() - pollData.timestamp < pollData.duration ) {

			console.log(pollData.validVoters)
			console.log(pollData.validVoters === [])

			if ( (pollData.validVoters === undefined) || (pollData.validVoters.length === 0) || (pollData.validVoters.includes(userID)) ) {

				index = pollData.votes.map(x => x.userID).indexOf(userID);

				if ( index > -1 ) { pollData.votes.splice(index, 1); };

				if ( !(votes.length === pollData.choices.length) ) {
					res.status(400).end();

				} else {
					var validVotes = [...Array(votes.length).keys()].map(i => pollData.choices[i].answers.includes(votes[i]));

					if ( !validVotes.every(x => x) ) {
						res.status(400).end();

					} else {
						let certificate = crypto.randomBytes(18).toString('hex')

						pollData.votes.push({
							username: username,
							userID: userID,
							votes: votes,
							certificate: certificate
						});

						savePoll(pollData);
						res.status(200).send({certificate: certificate}).end();
					};
				};
			} else {
				res.status(401).end();
			};
		} else {
			res.status(403).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

app.delete('/api/polls/:pollID', function(req, res, next) {

	var pollID = req.params.pollID,
	    privateKey = req.body.privateKey;

	if ( checkPoll(pollID) ) {
		let pollData = readPoll(pollID);
		if ( pollData.privateKey === privateKey ) {
			fs.unlinkSync(path.join(__dirname, pollDirectory, pollID + '.json'));
			res.status(200).end();

		} else {
			res.status(401).end();
		};
	} else {
		res.status(404).end();
	};

	next();

});

/*
 *
 *
 *
 */

// Activar el servidor solamente si se activa directamente este módulo
if (require.main === module) {

	// start the HTTP/2 server with express
	var server = spdy.createServer(options, app);
	server.listen(PORT);
}

// Exportar la aplicación
module.exports = app;
